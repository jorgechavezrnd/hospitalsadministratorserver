package com.hospitaladministrator.crud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hospitaladministrator.crud.dto.ImageDto;
import com.hospitaladministrator.crud.dto.Message;
import com.hospitaladministrator.crud.dto.PatientDto;
import com.hospitaladministrator.crud.entity.Hospital;
import com.hospitaladministrator.crud.entity.Patient;
import com.hospitaladministrator.crud.service.HospitalService;
import com.hospitaladministrator.crud.service.PatientService;
import com.hospitaladministrator.crud.util.ImageUtils;

@RestController
@RequestMapping("/patient")
@CrossOrigin
public class PatientController {
	
	@Autowired
	private PatientService patientService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@GetMapping("/list")
	public ResponseEntity<List<Patient>> list() {
		List<Patient> list = patientService.list();
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/picture/{id}")
	public ResponseEntity<?> getProfilePicture(@PathVariable("id") int id) {
		if (!patientService.existsById(id))
			return new ResponseEntity<>(new Message("Patient not found"), HttpStatus.NOT_FOUND);
		
		Patient patient = patientService.findById(id).get();
		
		byte[] imageBytes = patient.getProfilePicture();
		
		ImageDto imageDto = new ImageDto();
		
		if (imageBytes != null)
			imageDto.setImageBytes(ImageUtils.decompressImageBytes(imageBytes));
		
		return new ResponseEntity<>(imageDto, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/create/{hospitalId}")
	public ResponseEntity<Message> create(@PathVariable("hospitalId") int hospitalId, @Valid @RequestBody PatientDto patientDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!hospitalService.existsById(hospitalId))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		Hospital hospital = hospitalService.findById(hospitalId).get();
		
		Patient patient = new Patient(patientDto.getFirstName(),
									  patientDto.getLastName(),
									  patientDto.getBirthday(),
									  patientDto.getAddress());
		patient.setHospital(hospital);
		
		patientService.save(patient);
		
		return new ResponseEntity<>(new Message("Patient created"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/{id}")
	public ResponseEntity<Message> update(@PathVariable("id") int id, @Valid @RequestBody PatientDto patientDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!patientService.existsById(id))
			return new ResponseEntity<>(new Message("Patient not found"), HttpStatus.NOT_FOUND);
		
		Patient patient = patientService.findById(id).get();
		
		patient.setFirstName(patientDto.getFirstName());
		patient.setLastName(patientDto.getLastName());
		patient.setBirthday(patientDto.getBirthday());
		patient.setAddress(patientDto.getAddress());
		
		patientService.save(patient);
		
		return new ResponseEntity<>(new Message("Patient updated"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/picture/{id}")
	public ResponseEntity<Message> updateProfilePicture(@PathVariable("id") int id, @RequestParam("imageFile") MultipartFile imageFile) {
		if (!patientService.existsById(id))
			return new ResponseEntity<>(new Message("Patient not found"), HttpStatus.NOT_FOUND);
		
		try {
			Patient patient = patientService.findById(id).get();
			patient.setProfilePicture(ImageUtils.compressImageBytes(imageFile.getBytes()));
			
			patientService.save(patient);
			
			return new ResponseEntity<>(new Message("Profile picture updated"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new Message("Error on update profile picture"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Message> delete(@PathVariable("id") int id) {
		if (!patientService.existsById(id))
			return new ResponseEntity<>(new Message("Patient not found"), HttpStatus.NOT_FOUND);
		
		patientService.delete(id);
		
		return new ResponseEntity<>(new Message("Patient deleted"), HttpStatus.OK);
	}
	
}
