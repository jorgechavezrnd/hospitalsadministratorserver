package com.hospitaladministrator.crud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hospitaladministrator.crud.dto.ImageDto;
import com.hospitaladministrator.crud.dto.Message;
import com.hospitaladministrator.crud.dto.SpecialtyDto;
import com.hospitaladministrator.crud.entity.Specialty;
import com.hospitaladministrator.crud.service.SpecialtyService;
import com.hospitaladministrator.crud.util.ImageUtils;

@RestController
@RequestMapping("/specialty")
@CrossOrigin
public class SpecialtyController {
	
	@Autowired
	private SpecialtyService specialtyService;
	
	@GetMapping("/list")
	public ResponseEntity<List<Specialty>> list() {
		List<Specialty> list = specialtyService.list();
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/image/{id}")
	public ResponseEntity<?> getImage(@PathVariable("id") int id) {
		if (!specialtyService.existsById(id))
			return new ResponseEntity<>(new Message("Specialty not found"), HttpStatus.NOT_FOUND);
		
		Specialty specialty = specialtyService.findById(id).get();
		
		byte[] imageBytes = specialty.getImage();
		
		ImageDto imageDto = new ImageDto();
		if (imageBytes != null)
			imageDto.setImageBytes(ImageUtils.decompressImageBytes(imageBytes));
		
		return new ResponseEntity<>(imageDto, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/create")
	public ResponseEntity<Message> create(@Valid @RequestBody SpecialtyDto specialtyDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		Specialty specialty = new Specialty(specialtyDto.getName());
		specialty.setDescription(specialtyDto.getDescription());
		
		specialtyService.save(specialty);
		
		return new ResponseEntity<>(new Message("Specialty created"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/{id}")
	public ResponseEntity<Message> update(@PathVariable("id") int id, @Valid @RequestBody SpecialtyDto specialtyDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!specialtyService.existsById(id))
			return new ResponseEntity<>(new Message("Specialty not found"), HttpStatus.NOT_FOUND);
		
		if (specialtyService.existsByName(specialtyDto.getName()) && specialtyService.findByName(specialtyDto.getName()).get().getId() != id)
			return new ResponseEntity<>(new Message("Specialty with this name already exists"), HttpStatus.BAD_REQUEST);
		
		Specialty specialty = specialtyService.findById(id).get();
		
		specialty.setName(specialtyDto.getName());
		specialty.setDescription(specialtyDto.getDescription());
		
		specialtyService.save(specialty);
		
		return new ResponseEntity<>(new Message("Specialty updated"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/image/{id}")
	public ResponseEntity<Message> updateImage(@PathVariable("id") int id, @RequestParam("imageFile") MultipartFile imageFile) {
		if (!specialtyService.existsById(id))
			return new ResponseEntity<>(new Message("Specialty not found"), HttpStatus.NOT_FOUND);
		
		try {
			Specialty specialty = specialtyService.findById(id).get();
			specialty.setImage(ImageUtils.compressImageBytes(imageFile.getBytes()));
			
			specialtyService.save(specialty);
			
			return new ResponseEntity<>(new Message("Image updated"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new Message("Could not update image"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Message> delete(@PathVariable("id") int id) {
		if (!specialtyService.existsById(id))
			return new ResponseEntity<>(new Message("Specialty not found"), HttpStatus.NOT_FOUND);
		
		specialtyService.delete(id);
		
		return new ResponseEntity<>(new Message("Specialty deleted"), HttpStatus.OK);
	}
	
}
