package com.hospitaladministrator.crud.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hospitaladministrator.crud.dto.DoctorDto;
import com.hospitaladministrator.crud.dto.ImageDto;
import com.hospitaladministrator.crud.dto.Message;
import com.hospitaladministrator.crud.entity.Doctor;
import com.hospitaladministrator.crud.entity.Hospital;
import com.hospitaladministrator.crud.entity.Specialty;
import com.hospitaladministrator.crud.service.DoctorService;
import com.hospitaladministrator.crud.service.HospitalService;
import com.hospitaladministrator.crud.util.ImageUtils;

@RestController
@RequestMapping("/doctor")
@CrossOrigin
public class DoctorController {
	
	@Autowired
	private DoctorService doctorService;
	
	@Autowired
	private HospitalService hospitalService;
	
	@GetMapping("/list")
	public ResponseEntity<List<Doctor>> list() {
		List<Doctor> list = doctorService.list();
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/picture/{id}")
	public ResponseEntity<?> getProfilePicture(@PathVariable("id") int id) {
		if (!doctorService.existsById(id))
			return new ResponseEntity<>(new Message("Doctor not found"), HttpStatus.NOT_FOUND);
		
		Doctor doctor = doctorService.findById(id).get();
		
		byte[] imageBytes = doctor.getProfilePicture();
		
		ImageDto imageDto =  new ImageDto();
		
		if (imageBytes != null)
			imageDto.setImageBytes(ImageUtils.decompressImageBytes(imageBytes));
		
		return new ResponseEntity<>(imageDto, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/create/{hospitalId}")
	public ResponseEntity<Message> create(@PathVariable("hospitalId") int hospitalId, @Valid @RequestBody DoctorDto doctorDto, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!hospitalService.existsById(hospitalId))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		Hospital hospital = hospitalService.findById(hospitalId).get();
		
		Doctor doctor = new Doctor(doctorDto.getFirstName(),
								   doctorDto.getLastName(),
								   doctorDto.getAddress());
		doctor.setHospital(hospital);
		if (doctorDto.getBirthday() != null)
			doctor.setBirthday(doctorDto.getBirthday());
		
		doctorService.save(doctor);
		
		return new ResponseEntity<>(new Message("Doctor created"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/{id}")
	public ResponseEntity<Message> update(@PathVariable("id") int id, @Valid @RequestBody DoctorDto doctorDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!doctorService.existsById(id))
			return new ResponseEntity<>(new Message("Doctor not found"), HttpStatus.NOT_FOUND);
		
		Doctor doctor = doctorService.findById(id).get();
		doctor.setFirstName(doctorDto.getFirstName());
		doctor.setLastName(doctorDto.getLastName());
		doctor.setAddress(doctorDto.getAddress());
		doctor.setBirthday(doctorDto.getBirthday());
		
		doctorService.save(doctor);
		
		return new ResponseEntity<>(new Message("Doctor updated"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/specialties/{id}")
	public ResponseEntity<Message> updateSpecialties(@PathVariable("id") int id, @RequestBody Set<Specialty> specialties) {
		if (!doctorService.existsById(id))
			return new ResponseEntity<>(new Message("Doctor not found"), HttpStatus.NOT_FOUND);
		
		Doctor doctor = doctorService.findById(id).get();
		doctor.setSpecialties(specialties);
		
		doctorService.save(doctor);
		
		return new ResponseEntity<>(new Message("Doctor's specialties updated"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/picture/{id}")
	public ResponseEntity<Message> updateProfilePicture(@PathVariable("id") int id, @RequestParam("imageFile") MultipartFile imageFile) {
		if (!doctorService.existsById(id))
			return new ResponseEntity<>(new Message("Doctor not found"), HttpStatus.NOT_FOUND);
		
		try {
			Doctor doctor = doctorService.findById(id).get();
			doctor.setProfilePicture(ImageUtils.compressImageBytes(imageFile.getBytes()));
			
			doctorService.save(doctor);
			
			return new ResponseEntity<>(new Message("Profile picture updated"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new Message("Error on update profile picture"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Message> delete(@PathVariable("id") int id) {
		if (!doctorService.existsById(id))
			return new ResponseEntity<>(new Message("Doctor not found"), HttpStatus.NOT_FOUND);
		
		doctorService.delete(id);
		
		return new ResponseEntity<>(new Message("Doctor deleted"), HttpStatus.OK);
	}
	
}
