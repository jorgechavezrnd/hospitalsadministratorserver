package com.hospitaladministrator.crud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hospitaladministrator.crud.dto.Message;
import com.hospitaladministrator.crud.dto.NoteDto;
import com.hospitaladministrator.crud.entity.Note;
import com.hospitaladministrator.crud.entity.Patient;
import com.hospitaladministrator.crud.service.NoteService;
import com.hospitaladministrator.crud.service.PatientService;

@RestController
@RequestMapping("/note")
@CrossOrigin
public class NoteController {
	
	@Autowired
	private NoteService noteService;
	
	@Autowired
	private PatientService patientService;
	
	@GetMapping("/list")
	public ResponseEntity<List<Note>> list() {
		List<Note> list = noteService.list();
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/create/{patientId}")
	public ResponseEntity<Message> create(@PathVariable("patientId") int patientId, @Valid @RequestBody NoteDto noteDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!patientService.existsById(patientId))
			return new ResponseEntity<>(new Message("Patient not found"), HttpStatus.NOT_FOUND);
		
		Patient patient = patientService.findById(patientId).get();
		
		Note note = new Note(noteDto.getText(), noteDto.getDate());
		note.setPatient(patient);
		
		noteService.save(note);
		
		return new ResponseEntity<>(new Message("Note created"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/{id}")
	public ResponseEntity<Message> update(@PathVariable("id") int id, @Valid @RequestBody NoteDto noteDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!noteService.existsById(id))
			return new ResponseEntity<>(new Message("Note not found"), HttpStatus.NOT_FOUND);
		
		Note note = new Note(noteDto.getText(), noteDto.getDate());
		
		noteService.save(note);
		
		return new ResponseEntity<>(new Message("Note updated"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Message> delete(@PathVariable("id") int id) {
		if (!noteService.existsById(id))
			return new ResponseEntity<>(new Message("Note not found"), HttpStatus.NOT_FOUND);
		
		noteService.delete(id);
		
		return new ResponseEntity<>(new Message("Note deleted"), HttpStatus.OK);
	}
	
}
