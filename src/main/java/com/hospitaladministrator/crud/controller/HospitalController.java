package com.hospitaladministrator.crud.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.hospitaladministrator.crud.dto.HospitalDto;
import com.hospitaladministrator.crud.dto.ImageDto;
import com.hospitaladministrator.crud.dto.Message;
import com.hospitaladministrator.crud.entity.Hospital;
import com.hospitaladministrator.crud.service.HospitalService;
import com.hospitaladministrator.crud.util.ImageUtils;

@RestController
@RequestMapping("/hospital")
@CrossOrigin
public class HospitalController {
	
	@Autowired
	private HospitalService hospitalService;
	
	@GetMapping("/list")
	public ResponseEntity<List<Hospital>> list() {
		List<Hospital> list = hospitalService.list();
		
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<?> getById(@PathVariable("id") int id) {
		if (!hospitalService.existsById(id))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		Hospital hospital = hospitalService.findById(id).get();
		
		return new ResponseEntity<>(hospital, HttpStatus.OK);
	}
	
	@GetMapping("/image/{id}")
	public ResponseEntity<?> getImage(@PathVariable("id") int id) {
		if (!hospitalService.existsById(id))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		Hospital hospital = hospitalService.findById(id).get();
		
		byte[] imageBytes = hospital.getImage();
		
		ImageDto imageDto = new ImageDto();
		if (imageBytes != null)
			imageDto.setImageBytes(ImageUtils.decompressImageBytes(imageBytes));
		
		return new ResponseEntity<>(imageDto, HttpStatus.OK);
	}
	
	@GetMapping("/find/name/{name}")
	public ResponseEntity<?> getByName(@PathVariable("name") String name) {
		if (!hospitalService.existsByName(name))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		Hospital hospital = hospitalService.findByName(name).get();
		
		return new ResponseEntity<>(hospital, HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping("/create")
	public ResponseEntity<Message> create(@Valid @RequestBody HospitalDto hospitalDto, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (hospitalService.existsByName(hospitalDto.getName()))
			return new ResponseEntity<>(new Message("Hospital with this name aldeady exists"),
										HttpStatus.BAD_REQUEST);
		
		Hospital hospital = new Hospital();
		hospital.setName(hospitalDto.getName());
		hospital.setAddress(hospitalDto.getAddress());
		
		hospitalService.save(hospital);
		
		return new ResponseEntity<>(new Message("Hospital created"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/{id}")
	public ResponseEntity<Message> update(@PathVariable("id") int id, @Valid @RequestBody HospitalDto hospitalDto,
										  BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (!hospitalService.existsById(id))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		if (hospitalService.existsByName(hospitalDto.getName()) && hospitalService.findByName(hospitalDto.getName()).get().getId() != id)
			return new ResponseEntity<>(new Message("Hospital with this name already exists"), HttpStatus.BAD_REQUEST);
		
		Hospital hospital = hospitalService.findById(id).get();
		hospital.setName(hospitalDto.getName());
		hospital.setAddress(hospitalDto.getAddress());
		
		hospitalService.save(hospital);
		
		return new ResponseEntity<>(new Message("Hospital updated"), HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PutMapping("/update/image/{id}")
	public ResponseEntity<Message> updateImage(@PathVariable("id") int id, @RequestParam("imageFile") MultipartFile imageFile) {
		if (!hospitalService.existsById(id))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		try {
			Hospital hospital = hospitalService.findById(id).get();
			hospital.setImage(ImageUtils.compressImageBytes(imageFile.getBytes()));
			
			hospitalService.save(hospital);
			
			return new ResponseEntity<>(new Message("Image updated"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new Message("Could not update image"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Message> delete(@PathVariable("id") int id) {
		if (!hospitalService.existsById(id))
			return new ResponseEntity<>(new Message("Hospital not found"), HttpStatus.NOT_FOUND);
		
		hospitalService.delete(id);
		
		return new ResponseEntity<>(new Message("Hospital deleted"), HttpStatus.OK);
	}
	
}
