package com.hospitaladministrator.crud.dto;

import javax.validation.constraints.NotBlank;

public class HospitalDto {
	
	@NotBlank(message = "Name is mandatory")
	private String name;
	
	@NotBlank(message = "Address is mandatory")
	private String address;
	
	public HospitalDto() {
	}
	
	public HospitalDto(@NotBlank String name, @NotBlank String address) {
		this.name = name;
		this.address = address;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
