package com.hospitaladministrator.crud.dto;

public class ImageDto {
	
	private byte[] imageBytes;
	
	public ImageDto() {
	}
	
	public ImageDto(byte[] imageBytes) {
		this.imageBytes = imageBytes;
	}

	public byte[] getImageBytes() {
		return imageBytes;
	}

	public void setImageBytes(byte[] imageBytes) {
		this.imageBytes = imageBytes;
	}
	
}
