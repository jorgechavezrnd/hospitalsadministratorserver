package com.hospitaladministrator.crud.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

public class PatientDto {
	
	@NotBlank(message = "First name is mandatory")
	private String firstName;
	
	@NotBlank(message = "Last name is mandatory")
	private String lastName;
	
	@NotBlank(message = "Birthday is mandatory")
	private Date birthday;
	
	@NotBlank(message = "Address is mandatory")
	private String address;
	
	public PatientDto() {
	}
	
	public PatientDto(@NotBlank String firstName,
					  @NotBlank String lastName,
					  @NotBlank Date birthday,
					  @NotBlank String address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.address = address;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
