package com.hospitaladministrator.crud.dto;

import javax.validation.constraints.NotBlank;

public class SpecialtyDto {
	
	@NotBlank(message = "Name is mandatory")
	private String name;
	
	private String description;
	
	public SpecialtyDto() {
	}

	public SpecialtyDto(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
