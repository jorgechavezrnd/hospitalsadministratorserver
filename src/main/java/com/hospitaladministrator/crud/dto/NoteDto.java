package com.hospitaladministrator.crud.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;

public class NoteDto {
	
	@NotBlank(message = "Text is mandatory")
	private String text;
	
	@NotBlank(message = "Date is mandatory")
	private Date date;
	
	public NoteDto() {
	}

	public NoteDto(@NotBlank String text,
				   @NotBlank Date date) {
		this.text = text;
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
