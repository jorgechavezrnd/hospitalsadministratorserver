package com.hospitaladministrator.crud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospitaladministrator.crud.entity.Hospital;
import com.hospitaladministrator.crud.repository.HospitalRepository;

@Service
@Transactional
public class HospitalService {
	
	@Autowired
	private HospitalRepository hospitalRepository;
	
	public List<Hospital> list() {
		return hospitalRepository.findAll();
	}
	
	public Optional<Hospital> findById(int id) {
		return hospitalRepository.findById(id);
	}
	
	public Optional<Hospital> findByName(String name) {
		return hospitalRepository.findByName(name);
	}
	
	public void save(Hospital hospital) {
		hospitalRepository.save(hospital);
	}
	
	public void delete(int id) {
		hospitalRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return hospitalRepository.existsById(id);
	}
	
	public boolean existsByName(String name) {
		return hospitalRepository.existsByName(name);
	}
	
}
