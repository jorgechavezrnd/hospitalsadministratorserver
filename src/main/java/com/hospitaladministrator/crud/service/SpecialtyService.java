package com.hospitaladministrator.crud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospitaladministrator.crud.entity.Specialty;
import com.hospitaladministrator.crud.repository.SpecialtyRepository;

@Service
@Transactional
public class SpecialtyService {
	
	@Autowired
	private SpecialtyRepository specialtyRepository;
	
	public List<Specialty> list() {
		return specialtyRepository.findAll();
	}
	
	public Optional<Specialty> findById(int id) {
		return specialtyRepository.findById(id);
	}
	
	public Optional<Specialty> findByName(String name) {
		return specialtyRepository.findByName(name);
	}
	
	public void save(Specialty specialty) {
		specialtyRepository.save(specialty);
	}
	
	public void delete(int id) {
		specialtyRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return specialtyRepository.existsById(id);
	}
	
	public boolean existsByName(String name) {
		return specialtyRepository.existsByName(name);
	}
	
}
