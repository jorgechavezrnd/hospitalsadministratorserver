package com.hospitaladministrator.crud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospitaladministrator.crud.entity.Note;
import com.hospitaladministrator.crud.repository.NoteRepository;

@Service
@Transactional
public class NoteService {
	
	@Autowired
	private NoteRepository noteRepository;
	
	public List<Note> list() {
		return noteRepository.findAll();
	}
	
	public Optional<Note> findById(int id) {
		return noteRepository.findById(id);
	}
	
	public void save(Note note) {
		noteRepository.save(note);
	}
	
	public void delete(int id) {
		noteRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return noteRepository.existsById(id);
	}
	
}
