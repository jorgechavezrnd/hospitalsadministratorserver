package com.hospitaladministrator.crud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospitaladministrator.crud.entity.Doctor;
import com.hospitaladministrator.crud.repository.DoctorRepository;

@Service
@Transactional
public class DoctorService {
	
	@Autowired
	private DoctorRepository doctorRepository;
	
	public List<Doctor> list() {
		return doctorRepository.findAll();
	}
	
	public Optional<Doctor> findById(int id) {
		return doctorRepository.findById(id);
	}
	
	public void save(Doctor doctor) {
		doctorRepository.save(doctor);
	}
	
	public void delete(int id) {
		doctorRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return doctorRepository.existsById(id);
	}
	
}
