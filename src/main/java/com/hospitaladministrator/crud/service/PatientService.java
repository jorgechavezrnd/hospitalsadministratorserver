package com.hospitaladministrator.crud.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospitaladministrator.crud.entity.Patient;
import com.hospitaladministrator.crud.repository.PatienRepository;

@Service
@Transactional
public class PatientService {
	
	@Autowired
	private PatienRepository patientRepository;
	
	public List<Patient> list() {
		return patientRepository.findAll();
	}
	
	public Optional<Patient> findById(int id) {
		return patientRepository.findById(id);
	}
	
	public void save(Patient patient) {
		patientRepository.save(patient);
	}
	
	public void delete(int id) {
		patientRepository.deleteById(id);
	}
	
	public boolean existsById(int id) {
		return patientRepository.existsById(id);
	}
	
}
