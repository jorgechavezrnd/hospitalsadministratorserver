package com.hospitaladministrator.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hospitaladministrator.crud.entity.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Integer> {
	
}
