package com.hospitaladministrator.crud.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hospitaladministrator.crud.entity.Specialty;

@Repository
public interface SpecialtyRepository extends JpaRepository<Specialty, Integer> {
	
	Optional<Specialty> findByName(String name);
	
	boolean existsByName(String name);
	
}
