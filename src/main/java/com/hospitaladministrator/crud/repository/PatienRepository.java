package com.hospitaladministrator.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hospitaladministrator.crud.entity.Patient;

@Repository
public interface PatienRepository extends JpaRepository<Patient, Integer> {
	
}
