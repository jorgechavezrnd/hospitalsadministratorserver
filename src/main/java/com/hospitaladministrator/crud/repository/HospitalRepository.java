package com.hospitaladministrator.crud.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hospitaladministrator.crud.entity.Hospital;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, Integer> {
	
	Optional<Hospital> findByName(String name);
	
	boolean existsByName(String name);
	
}
