package com.hospitaladministrator.crud.security.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hospitaladministrator.crud.dto.Message;
import com.hospitaladministrator.crud.security.dto.JwtDto;
import com.hospitaladministrator.crud.security.dto.LoginUser;
import com.hospitaladministrator.crud.security.dto.NewUser;
import com.hospitaladministrator.crud.security.entity.Role;
import com.hospitaladministrator.crud.security.entity.User;
import com.hospitaladministrator.crud.security.enums.RoleName;
import com.hospitaladministrator.crud.security.jwt.JwtProvider;
import com.hospitaladministrator.crud.security.service.RoleService;
import com.hospitaladministrator.crud.security.service.UserService;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private JwtProvider jwtProvider;
	
	@PostMapping("/create")
	public ResponseEntity<Message> create(@Valid @RequestBody NewUser newUser, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		if (userService.existsByUsername(newUser.getUsername()))
			return new ResponseEntity<>(new Message("Username already exists"), HttpStatus.BAD_REQUEST);
		
		User user = new User(newUser.getName(), newUser.getUsername(), passwordEncoder.encode(newUser.getPassword()));
		
		Set<Role> roles = new HashSet<>();
		roles.add(roleService.getByRoleName(RoleName.ROLE_USER).get());
		
		if (newUser.getRoles() != null && newUser.getRoles().contains("admin"))
			roles.add(roleService.getByRoleName(RoleName.ROLE_ADMIN).get());
		
		user.setRoles(roles);
		
		userService.save(user);
		
		return new ResponseEntity<>(new Message("User created"), HttpStatus.CREATED);
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> login(@Valid @RequestBody LoginUser loginUser, BindingResult bindingResult) {
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(new Message(bindingResult.getFieldError().getDefaultMessage()),
										HttpStatus.BAD_REQUEST);
		
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
											loginUser.getUsername(),
											loginUser.getPassword())
										);
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtProvider.generateToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());
		
		return new ResponseEntity<>(jwtDto, HttpStatus.OK);
	}
	
}
