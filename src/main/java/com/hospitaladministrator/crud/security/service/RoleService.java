package com.hospitaladministrator.crud.security.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hospitaladministrator.crud.security.entity.Role;
import com.hospitaladministrator.crud.security.enums.RoleName;
import com.hospitaladministrator.crud.security.repository.RoleRepository;

@Service
@Transactional
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	public Optional<Role> getByRoleName(RoleName roleName) {
		return roleRepository.findByRoleName(roleName);
	}
	
	public void save(Role role) {
		roleRepository.save(role);
	}
	
}
