package com.hospitaladministrator.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalsadministratorserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(HospitalsadministratorserverApplication.class, args);
	}

}
