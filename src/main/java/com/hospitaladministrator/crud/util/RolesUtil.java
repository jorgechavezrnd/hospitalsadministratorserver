package com.hospitaladministrator.crud.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.hospitaladministrator.crud.security.entity.Role;
import com.hospitaladministrator.crud.security.enums.RoleName;
import com.hospitaladministrator.crud.security.service.RoleService;

@Component
public class RolesUtil implements CommandLineRunner {
	
	@Autowired
	private RoleService roleService;
	
	@Override
	public void run(String... args) throws Exception {
		
		if (!roleService.getByRoleName(RoleName.ROLE_ADMIN).isPresent())
			roleService.save(new Role(RoleName.ROLE_ADMIN));
		
		if (!roleService.getByRoleName(RoleName.ROLE_USER).isPresent())
			roleService.save(new Role(RoleName.ROLE_USER));
		
	}
	
}
