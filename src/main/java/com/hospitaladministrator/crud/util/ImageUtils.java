package com.hospitaladministrator.crud.util;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImageUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ImageUtils.class);
	
	public static byte[] compressImageBytes(byte[] imageBytes) {
		Deflater deflater = new Deflater();
		deflater.setInput(imageBytes);
		deflater.finish();
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(imageBytes.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		
		try {
			outputStream.close();
		} catch (Exception e) {
			LOGGER.error("Error on compress image bytes: ", e);
		}
		
		LOGGER.info("Compressed image byte size: " + outputStream.toByteArray().length);
		
		return outputStream.toByteArray();
	}
	
	public static byte[] decompressImageBytes(byte[] imageBytes) {
		Inflater inflater = new Inflater();
		inflater.setInput(imageBytes);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(imageBytes.length);
		byte[] buffer = new byte[1024];
		
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (Exception e) {
			LOGGER.error("Error on decompress image bytes: ", e);
		}
		
		return outputStream.toByteArray();
	}
	
}
